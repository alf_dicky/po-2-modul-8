import java.io.*;
public class WriteFile {
	public WriteFile() {		
	}	
	public static void main(String[] args)throws IOException {
		System.out.println("What is the name of the file to be written to?");
		String fileName;
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		fileName = input.readLine();
		System.out.println("Enter data to write to  "+fileName+" .....");
		System.out.println("Type q$ to end.");
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(fileName);
		}catch (FileNotFoundException e) {
			System.out.println("File not found.");
		}
		try {
			boolean done = false;
			int data;
			do {
				data = input.read();
				if((char)data == 'q') {
					data = input.read();
					if((char) data == '$') {
						done = true;
					}else {
						fos.write('q');
						fos.write(data);
					}
				}else {
					fos.write(data);
				}
			}while(!done);
		}catch (IOException e) {
			System.out.println("Problem is reading from the file!");
		}
	}
}

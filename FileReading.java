import java.io.*;
public class FileReading {
	public static void main(String[] args) throws IOException{
		String line = "" , fileContent = "";
		try {
			BufferedReader fileinput = new BufferedReader(new FileReader(new File("D:/coba.txt")));
			line = fileinput.readLine();
			fileContent = line + "\n";
			while(line != null) {
				line = fileinput.readLine();
				if(line != null)
					fileContent += line +"\n";
			}
			fileinput.close();
		}catch (EOFException e) {
			System.out.println("No More Line to read");
			System.exit(0);
		}catch (IOException e) {
			System.out.println("Error Reading File!");
			System.exit(0);
		}
		System.out.println(fileContent);
	}
}

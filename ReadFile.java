import java.io.*;
public class ReadFile {
	public ReadFile() {
	}	
	public static void main(String[] args) throws IOException {
		System.out.println("What is the name of the file to read form?");
		String fileName;
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		fileName = input.readLine();
		System.out.println("Now Reading from "+fileName+".....");
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(fileName);
		}catch (FileNotFoundException e) {
			System.out.println("File not found.");
		}		
		try {
			char data;
			int temp;
			do {
				temp = fis.read();
				data = (char) temp;
				if(temp != -1) {
					System.out.print(data);
				}
			}while(temp != -1);
		}catch (IOException e) {
			System.out.println("Problem in reading from the file.");
		}catch (NullPointerException e) {			
			}
	}
}
